package mickael.cianamea.petitmuseceri;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;

public class ItemActivity extends AppCompatActivity {

    public static Context context;
    ImageView imageItem;
    TextView nom;
    TextView marque;
    TextView description;
    TextView dateFabrication;
    TextView working;
    LinearLayout caracteristique;
    LinearLayout categories;
    LinearLayout decennies;
    LinearLayout images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        String position = "";
        if (intent.hasExtra("positionItem")) {
            position = intent.getStringExtra("positionItem");
        }
        Item item = ItemListe.itemList.get(Integer.parseInt(position));

        nom = (TextView) findViewById(R.id.nameItem);
        imageItem = (ImageView) findViewById(R.id.imageViewItem);
        marque = (TextView) findViewById(R.id.marqueItem);
        description = (TextView) findViewById(R.id.descritionItem);
        dateFabrication = (TextView) findViewById(R.id.anneFabricationItem);
        working = (TextView) findViewById(R.id.fonctionnelItem);
        caracteristique = (LinearLayout) findViewById(R.id.caracteristiqueItem);
        categories = (LinearLayout) findViewById(R.id.categorieItem);
        decennies = (LinearLayout) findViewById(R.id.anneCommercialisation);
        images = (LinearLayout) findViewById(R.id.imagesItems);


        nom.setText(item.getName());

        // Image
        if(item.getImage() == null){
            DownloadImages downloadImages = new DownloadImages((ImageView) imageItem, MainActivity.context);
            URL url = null;
            try {
                url = WebServiceUrl.getItemImagesListe((item.getId()));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            downloadImages.execute(url);
        }

        marque.setText(item.getMarque());
        description.setText(item.getDescription());
        dateFabrication.setText(String.valueOf(item.getDateCreation()));

        // Working
        if (item.isWorking()){
            working.setText("Oui");
        }
        else working.setText("Non");

        // Caracteristique
        if(item.getCaracteristique() != null){
            for (int i = 0; i < item.getCaracteristique().size(); i++){
                TextView textView = new TextView(this);
                textView.setText(item.getCaracteristique().get(i));
                textView.setTextColor(Color.WHITE);
                textView.setBottom(20);
                caracteristique.addView(textView);
            }
        }

        // Categories
        if(item.getCategories() != null){
            for (int i = 0; i < item.getCategories().size(); i++){
                TextView textView = new TextView(this);
                textView.setText(item.getCategories().get(i));
                textView.setTextColor(Color.WHITE);
                textView.setBottom(20);
                categories.addView(textView);
            }
        }

        // Commercialisation
        if(item.getDateCommercialisation() != null){
            for (int i = 0; i < item.getDateCommercialisation().size(); i++){
                TextView textView = new TextView(this);
                textView.setText(String.valueOf(item.getDateCommercialisation().get(i)));
                textView.setTextColor(Color.WHITE);
                textView.setBottom(20);
                decennies.addView(textView);
            }
        }

        // Liste d'images
        if(item.getPictures() != null){
            for(String picture : item.getPictures())
            for (int i = 0; i < item.getPictures().size(); i++){
                ImageView image = new ImageView(this);
                DownloadImages downloadImages = new DownloadImages((ImageView) image, MainActivity.context);
                URL url = null;
                try {
                    url = WebServiceUrl.getObjetImagesId(item.getId(),item.getPicture(i));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                downloadImages.execute(url);
                image.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                images.addView(image);
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //2 - Inflate the menu and add it to the Toolbar
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_search:
                return true;
            case R.id.alpha:
                Collections.sort(ItemListe.itemList, Item.comparatorAlpha);
                Intent refresh = new Intent(context, MainActivity.class);
                context.startActivity(refresh);
                return true;
            case R.id.chrono:
                Collections.sort(ItemListe.itemList, Item.comparatorChrono);
                Intent refresh2 = new Intent(context, MainActivity.class);
                context.startActivity(refresh2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
