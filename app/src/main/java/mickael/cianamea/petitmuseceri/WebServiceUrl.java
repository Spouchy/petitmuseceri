package mickael.cianamea.petitmuseceri;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {


    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";


    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }

    // Build URL to get information for a specific object
    private static final String SEARCH_ITEMS = "items";
    public static URL getItem(String idItem) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEMS)
                .appendPath(idItem);
        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String IMAGE_LISTE = "thumbnail";
    public static URL getItemImagesListe(String idItems) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEMS)
                .appendPath(idItems)
                .appendPath(IMAGE_LISTE);
        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String IMAGES = "images";
    public static URL getObjetImagesId(String idItems, String idImage) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEMS)
                .appendPath(idItems)
                .appendPath(IMAGES)
                .appendPath(idImage);
        URL url = new URL(builder.build().toString());
        return url;
    }


    // Get Catalog
    private static final String CATALOG = "catalog";
    public static URL getCatalog() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(CATALOG);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get All IDS
    private static final String ALL_IDS = "ids";
    public static URL getAllIds() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ALL_IDS);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get All Categories
    private static final String ALL_CATEGORIES = "categories";
    public static URL getAllCategories() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ALL_CATEGORIES);
        URL url = new URL(builder.build().toString());
        return url;
    }

}
