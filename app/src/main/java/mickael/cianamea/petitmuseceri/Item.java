package mickael.cianamea.petitmuseceri;

import java.util.Comparator;
import java.util.List;

public class Item {

    private String id;
    private String name;
    private String marque;
    private Integer dateCreation;
    private List<Integer> dateCommercialisation;
    private List<String> categories;
    private String description;
    private List<String> caracteristique;
    private boolean working;
    private List<String> pictures;
    private String image;


    public Item(String id, String name, String marque, Integer dateCreation, List<Integer> dateCommercialisation, List<String> categories, String description, List<String> caracteristique, boolean working, List<String> pictures, String image) {
        this.id = id;
        this.name = name;
        this.marque = marque;
        this.dateCreation = dateCreation;
        this.dateCommercialisation = dateCommercialisation;
        this.categories = categories;
        this.description = description;
        this.caracteristique = caracteristique;
        this.working = working;
        this.pictures = pictures;
        this.image = image;
    }

    public Item(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public Integer getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Integer dateCreation) {
        this.dateCreation = dateCreation;
    }

    public List<Integer> getDateCommercialisation() {
        return dateCommercialisation;
    }

    public void setDateCommercialisation(List<Integer> dateCommercialisation) {
        this.dateCommercialisation = dateCommercialisation;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getCaracteristique() {
        return caracteristique;
    }

    public void setCaracteristique(List<String> caracteristique) {
        this.caracteristique = caracteristique;
    }

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public List<String> getPictures() {
        return pictures;
    }

    public String getPicture(int i){
        return pictures.get(i);
    }

    public void setPictures(List<String> pictures) {
        this.pictures = pictures;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    //Compare pour Tri alphabetique
    public static Comparator<Item> comparatorAlpha = new Comparator<Item>() {
        @Override
        public int compare(Item o1, Item o2) {
            return o1.getName().compareTo(o2.getName());
        }
    };

    //Compare pour Tri Chronologique
    public static Comparator<Item> comparatorChrono = new Comparator<Item>() {
        @Override
        public int compare(Item o1, Item o2) {
            if (o1.getDateCreation() != null && o2.getDateCreation() != null) {
                return (int) (o1.getDateCreation() - o2.getDateCreation());
            }
            return 0;
        }
    };
}
