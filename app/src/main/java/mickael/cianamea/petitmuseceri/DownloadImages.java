package mickael.cianamea.petitmuseceri;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;

public class DownloadImages extends AsyncTask<URL, Void, Bitmap>{
    ImageView imageView;
    Context context;

    public DownloadImages(ImageView imageView, Context context) {
        this.imageView = imageView;
        this.context = context;
    }

    protected Bitmap doInBackground(URL... urls) {
        URL imageURL = urls[0];
        String url = String.valueOf(imageURL);
        Bitmap image = null;
        try {
            InputStream in = new java.net.URL(url).openStream();
            image = BitmapFactory.decodeStream(in);

        } catch (Exception e) {
            Log.e("Erreur Message", e.getMessage());
            e.printStackTrace();
        }
        return image;
    }

    protected void onPostExecute(Bitmap result) {
        imageView.setImageBitmap(result);
    }
}
