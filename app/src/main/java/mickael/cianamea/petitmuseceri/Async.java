package mickael.cianamea.petitmuseceri;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Async extends AsyncTask<URL, Integer, List<Item>> {

    Context context;

    public Async(Context context) {
        this.context = context;
    }

    @Override
    protected List<Item> doInBackground(URL... urls) {
        try {
            JSONResponseHandlerItem jsonResponseHandlerItem = new JSONResponseHandlerItem(ItemListe.itemList);
            HttpURLConnection urlConnection = (HttpURLConnection) urls[0].openConnection();
            InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
            jsonResponseHandlerItem.readJsonStream(inputStream);
        }  catch (MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
        //Collections.sort(ItemListe.itemList, Item.comparatorAlpha);
        return ItemListe.itemList;
    }

    @Override
    protected void onPostExecute(List<Item> itemList) {
        ItemListe.itemList = itemList;
        Intent refresh = new Intent(context, MainActivity.class);
        context.startActivity(refresh);
    }
}
