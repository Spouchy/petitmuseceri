package mickael.cianamea.petitmuseceri;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;




public class JSONResponseHandlerItem {

    private static final String TAG = JSONResponseHandlerItem.class.getSimpleName();

    private Item item;


    public JSONResponseHandlerItem(List<Item> itemList){
        //ItemListe.itemList = itemList;
    }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readItem(reader);
        } finally {
            reader.close();
        }
    }

    public void readItem(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name != null) {
                this.item = new Item();
                item.setId(name);
                name = null;
                readArrayItem(reader);
                if (item.getDateCreation() == null) item.setDateCreation(0);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayItem(JsonReader reader) throws IOException {
        reader.beginObject();
        int nb = 0;
        while (reader.hasNext() ) {
            String name = reader.nextName();
            if (nb==0) {
                if (name.equals("technicalDetails")) {
                    List<String> listTmp = new ArrayList<String>();
                    reader.beginArray();
                    while (reader.hasNext()) {
                        listTmp.add(reader.nextString());
                    }
                    reader.endArray();
                    item.setCaracteristique(listTmp);
                }
                else if (name.equals("pictures")){
                    List<String> listTmp = new ArrayList<String>();
                    reader.beginObject();
                    while (reader.hasNext()){
                        listTmp.add(reader.nextName());
                        reader.skipValue();
                    }
                    reader.endObject();
                    item.setPictures(listTmp);
                }
                else if (name.equals("name")) {
                    item.setName(reader.nextString());
                }
                else if (name.equals("brand")) {
                    item.setMarque(reader.nextString());
                }
                else if (name.equals("year")) {
                    item.setDateCreation(reader.nextInt());
                }
                else if (name.equals("description")) {
                    item.setDescription(reader.nextString());
                }
                else if (name.equals("timeFrame")){
                    List<Integer> listTmp = new ArrayList<Integer>();
                    reader.beginArray();
                    while (reader.hasNext()) {
                        listTmp.add(reader.nextInt());
                    }
                    reader.endArray();
                    item.setDateCommercialisation(listTmp);
                }
                else if (name.equals("categories")){
                    List<String> listTmp = new ArrayList<String>();
                    reader.beginArray();
                    while (reader.hasNext()) {
                        listTmp.add(reader.nextString());
                    }
                    reader.endArray();
                    item.setCategories(listTmp);
                }
                else if (name.equals("working")){
                    item.setWorking(reader.nextBoolean());
                }
                else {
                    reader.skipValue();
                }
            }
            else {
                reader.skipValue();
            }
        }
        reader.endObject();
        nb++;
        ItemListe.itemList.add(item);
    }

}
