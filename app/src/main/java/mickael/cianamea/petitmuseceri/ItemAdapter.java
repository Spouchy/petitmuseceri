package mickael.cianamea.petitmuseceri;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.itemViewHolder> implements Filterable {

    public int positionItem;
    private List<Item> listAll;
    private List<Item> listItemRecherche;


    public static class itemViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{
        ImageView imageItem;
        TextView nom;
        TextView marque;
        TextView date;
        LinearLayout categorieRow;
        LinearLayout row;

        public itemViewHolder(@NonNull View itemView) {
            super(itemView);
            imageItem = itemView.findViewById(R.id.imageView);
            nom = itemView.findViewById(R.id.name);
            marque = itemView.findViewById(R.id.marque);
            date = itemView.findViewById(R.id.date);
            categorieRow = itemView.findViewById(R.id.categorieRow);
            row = itemView.findViewById(R.id.rowLayout);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final int position = this.getAdapterPosition();
            Intent itemActivity = new Intent(MainActivity.context, ItemActivity.class);
            itemActivity.putExtra("positionItem", String.valueOf(position));
            Context context = MainActivity.context;
            context.startActivity(itemActivity);
        }
    }

    public ItemAdapter(List<Item> ItemList) {
        listAll = ItemList;
        listItemRecherche = new ArrayList<>(ItemList);

    }

    @NonNull
    @Override
    public itemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        itemViewHolder itemViewHolder = new itemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull itemViewHolder holder, int position) {
        //Item item = ItemListe.itemList.get(position);
        Item item = listAll.get(position);
        positionItem = position;
        holder.nom.setText(item.getName());
        holder.marque.setText(item.getMarque());
        if (item.getDateCreation() != null && item.getDateCreation() != 0){
            String dateString = String.valueOf(item.getDateCreation());
            holder.date.setText(dateString);
        } else  holder.date.setText("");

        // Categories
        if(item.getCategories() != null){
            holder.categorieRow.removeAllViews();
            for (int i = 0; i < item.getCategories().size(); i++){
                TextView textView = new TextView(MainActivity.context);
                textView.setText(item.getCategories().get(i));
                textView.setGravity(Gravity.RIGHT | Gravity.TOP);
                holder.categorieRow.addView(textView);
            }
        }

        //if(ItemListe.itemList.get(position).getImage() == null){
        if(listAll.get(position).getImage() == null){
            DownloadImages downloadImages = new DownloadImages((ImageView) holder.imageItem, MainActivity.context);
            URL url = null;
            try {
                //url = WebServiceUrl.getItemImagesListe((ItemListe.itemList.get(position).getId()));
                url = WebServiceUrl.getItemImagesListe((listAll.get(position).getId()));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            downloadImages.execute(url);
        }
    }

    @Override
    public int getItemCount() {
        //return ItemListe.taille();
        return listAll.size();
    }

    public int getPosition(){
        return positionItem;
    }

    public List<String> getListCategorie(){
        List<String> listCatTmp = new ArrayList<String>();
        boolean isPresent = false;
        for (int i = 0; i < ItemListe.taille(); i++){
            if (ItemListe.itemList.get(i).getCategories() != null) {
                for (int y = 0; y < ItemListe.itemList.get(i).getCategories().size(); y++) {
                    for (int z = 0; z < listCatTmp.size(); z++) {
                        if (listCatTmp.get(z) == ItemListe.itemList.get(i).getCategories().get(y)) {
                            isPresent = true;
                            break;
                        }
                    }
                    if (isPresent == false) {
                        listCatTmp.add(ItemListe.itemList.get(i).getCategories().get(y));
                        isPresent = false;
                    }
                }
            }
        }
        return listCatTmp;
    }

    public List<Item> listParCategorie(){
        List<String> listcategorie = getListCategorie();
        List<Item> listItemCategorie = new ArrayList<Item>();
        for (int i = 0; i < listcategorie.size(); i++){
            Item item = new Item();
            item.setName(listcategorie.get(i));
            listItemCategorie.add(item);
            for (int y = 0; y < ItemListe.taille(); y++){
                for (int z = 0; z < ItemListe.itemList.get(y).getCategories().size(); z++){
                    if(ItemListe.itemList.get(y).getCategories().get(z) == listcategorie.get(i)){
                        listItemCategorie.add(ItemListe.itemList.get(y));
                    }
                }
            }
        }
        listAll.clear();
        listAll.addAll((List) listItemCategorie);
        notifyDataSetChanged();
        return listItemCategorie;
    }

    @Override
    public Filter getFilter() {
        return filtre;
    }

    private Filter filtre = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Item> listFiltrer = new ArrayList<>();
            if (constraint == null || constraint.length() == 0){
                //listFiltrer.addAll(ItemListe.itemList);
                listFiltrer.addAll(listItemRecherche);
            }
            else {
                String stringSearch = constraint.toString().toLowerCase().trim();
                for (int i = 0; i < listItemRecherche.size(); i++){
                    if (listItemRecherche.get(i).getName().toLowerCase().contains(stringSearch)){
                        listFiltrer.add(listItemRecherche.get(i));
                    }
                    else if (listItemRecherche.get(i).getDateCreation().toString().toLowerCase().contains(stringSearch)){
                        listFiltrer.add(listItemRecherche.get(i));
                    }
                    else if (listItemRecherche.get(i).getId().toLowerCase().contains(stringSearch)) {
                            listFiltrer.add(listItemRecherche.get(i));
                    }
                    else if (listItemRecherche.get(i).getMarque() != null){
                        if (listItemRecherche.get(i).getMarque().toLowerCase().contains(stringSearch)) {
                            listFiltrer.add(listItemRecherche.get(i));
                        }
                    }
                    else if (listItemRecherche.get(i).getDescription() != null){
                        if (listItemRecherche.get(i).getDescription().toLowerCase().contains(stringSearch)) {
                            listFiltrer.add(listItemRecherche.get(i));
                        }
                    }
                    else if (listItemRecherche.get(i).getCategories() != null){
                        for (int y = 0; y < listItemRecherche.get(i).getCategories().size(); y++){
                            if (listItemRecherche.get(i).getCategories().get(y).toLowerCase().contains(stringSearch)) {
                                listFiltrer.add(listItemRecherche.get(i));
                            }
                        }
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = listFiltrer;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listAll.clear();
            listAll.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

}
